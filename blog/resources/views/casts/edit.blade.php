<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Document</title>
</head>

<body>
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Edit Casts {{$cast->id}}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="/casts/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
      <div class="card-body">
        <div class="form-group">
          <label for="nama">Nama</label><br>
          <input type="text" class="form-control" name="nama" value=" {{ old('nama',$cast->nama) }} " id="nama" placeholder="Masukkan Nama">
          @error('nama')
             <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div><br>
        <div class="form-group">
          <label for="umur">Umur</label><br>
          <input type="number" class="form-control" name="umur" value="{{ old('umur', $cast->umur) }}" id="umur" placeholder="Masukkan Umur">
          @error('umur')
             <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div><br>
        <div class="form-group">
          <label for="bio">Bio</label><br>
          <textarea rows="4" cols="50" class="form-control" name="bio" id="bio">{{ old('bio', $cast->bio) }}
          </textarea>
          @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
      </div><br>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Edit</button>
      </div>
    </form>
  </div>
</body>

</html>