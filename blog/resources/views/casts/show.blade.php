@extends('master')

@section('content')
<div class="container-fluid mt-3">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th style="width: 200px">Nama</th>
                <th style="width: 100px">Umur</th>
                <th style="width: 400px">Bio</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td> {{ $cast->nama }} </td>
                <td> {{ $cast->umur }} </td>
                <td> {{ $cast->bio }} </td>
            </tr>
        </tbody>
    </table>
    
</div>
@endsection