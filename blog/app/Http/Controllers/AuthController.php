<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('Register');
    }

    public function welcome(){
        return view('Welcome');
    }

    public function welcome_post(Request $request){
        $nama = $request["nama"];
        $namaPanjang = $request["namaPanjang"];
        return view('Welcome', compact('nama', 'namaPanjang'));
    }
}
