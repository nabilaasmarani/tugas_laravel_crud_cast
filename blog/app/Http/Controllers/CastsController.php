<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastsController extends Controller
{
    public function create() {
        return view('casts.create');
    }

    public function store(Request $request) {
        //dd($request->all());
        $request->validate([
            "nama" =>'required|unique:casts',
            "umur" =>'required',
            "bio" =>'required'
        ]);
        $query = DB::table('casts')->insert([
            "nama" =>$request["nama"],
            "umur" =>$request["umur"],
            "bio" =>$request["bio"]
        ]);

        return redirect('/casts')->with('success', 'New cast has been added');
    }

    public function index() {
        $casts = DB::table('casts')->get();
        //dd($casts);
        return view('casts.index', compact('casts'));
    }

    public function show($id) {
        $cast = DB::table('casts')->where('id', $id)->first();
        //dd($cast);
        return view('casts.show', compact('cast'));
    }

    public function edit($id) {
        $cast = DB::table('casts')->where('id', $id)->first();

        return view('casts.edit', compact('cast'));
    }

    public function update($id, Request $request) {
        $request->validate([
            "nama" =>'required|unique:casts',
            "umur" =>'required',
            "bio" =>'required'
        ]);

        $query = DB::table('casts')
                    ->where('id', $id)
                    ->update([
                        'nama' => $request['nama'],
                        'umur' => $request['umur'],
                        'bio' => $request['bio']
                    ]);
        return redirect('/casts')->with('success', 'Update Successful.');
    }

    public function destroy($id) {
        $query = DB::table('casts')->where('id', $id)->delete();
        return redirect('/casts')->with('success', 'Cast has been deleted.');
    }
}
